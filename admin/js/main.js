$(document).ready(function () {

    var myStorage = window.localStorage;

    jQuery.ajaxPrefilter(function (options) {
        if (options.crossDomain && jQuery.support.cors) {
            options.url = 'https://cors-anywhere.herokuapp.com/' + options.url;
        }
    });

    function logout() {
        $.ajax({
            url: 'https://gaquad-api.herokuapp.com/auth/logout/' + myStorage.getItem('token'),
            method: 'delete',
            headers: {
                'Authorization': 'Bearer ' + myStorage.getItem('token')
            }
        }).done(function (response) {
            console.log(response);
            myStorage.clear();
            
            alert("Logout efetuado com sucesso! Você será redirecionado!");
            window.location.replace("/login.html");
        }).fail(function (error) {
            console.error(error);
            if (error.responseJSON) {
                alert(error.responseJSON.error.description)
            }
        });
    }

    $('#logout').on("click", logout);


});